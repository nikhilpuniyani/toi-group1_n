package com.perfectotoi.pages;

import java.util.List;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomeTestPageIOS extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "naivgationbar.homepage.ios")
	private QAFWebElement naivgationbarHomepageIos;
	@FindBy(locator = "list.tab.homepage.ios")
	private List<QAFWebElement> listTabHomepageIos;
	@FindBy(locator = "contentdesc.all.ios")
	private QAFWebElement contentdescAllIos;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getNaivgationbarHomepageIos() {
		return naivgationbarHomepageIos;
	}

	public List<QAFWebElement> getListTabHomepageIos() {
		return listTabHomepageIos;
	}

	public QAFWebElement getContentdescAllIos() {
		return contentdescAllIos;
	}

}
