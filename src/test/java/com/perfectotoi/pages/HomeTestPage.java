package com.perfectotoi.pages;

import java.util.List;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomeTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "skip.homepage")
	private QAFWebElement skipHomepage;
	@FindBy(locator = "sensex.section.homepage")
	private QAFWebElement sensexSectionHomepage;
	@FindBy(locator = "naivgationbar.homepage")
	private QAFWebElement naivgationbarHomepage;
	@FindBy(locator = "list.tab.homepage")
	private List<QAFWebElement> listTabHomepage;
	@FindBy(locator = "story.text")
	private QAFWebElement story;
	@FindBy(locator = "star.icon")
	private QAFWebElement star;
	@FindBy(locator = "menu.icon")
	private QAFWebElement menu_icon;
	@FindBy(locator = "saved_stories_icon")
	private QAFWebElement saved_stories_icon;
	@FindBy(locator = "back.button")
	private QAFWebElement back_button;
	@FindBy(locator = "saved_story.text")
	private QAFWebElement saved_story;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getSkipHomepage() {
		return skipHomepage;
	}

	public QAFWebElement getSensexSectionHomepage() {
		return sensexSectionHomepage;
	}

	public QAFWebElement getNaivgationbarHomepage() {
		return naivgationbarHomepage;
	}

	public List<QAFWebElement> getListTabHomepage() {
		return listTabHomepage;
	}

	public QAFWebElement getStory() {
		return story;
	}

	public QAFWebElement getStar() {
		return star;
	}

	public QAFWebElement getMenu_icon() {
		return menu_icon;
	}

	public QAFWebElement getSaved_stories_icon() {
		return saved_stories_icon;
	}

	public QAFWebElement getBack_button() {
		return back_button;
	}

	public QAFWebElement getSaved_story() {
		return saved_story;
	}
	
	

}
